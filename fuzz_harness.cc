#include "gtest/gtest.h"
#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <stdio.h>
#include "googletest/samples/sample1.h"


//int main(int argc, char** argv){
extern "C" int LLVMFuzzerTestOneInput(const uint8_t * data, size_t size) {
    int test_value = 0;

    if (size < sizeof(int)) // too short for a full int so convert manually
    {
        for(int i = 0; i < size; i++)
        {
            test_value = test_value << 8;
            test_value = test_value | (data[i] & 0xFF);
        }

    }
    else
        test_value = *(int*)data;

    IsPrime(test_value);

    return 0;
}


